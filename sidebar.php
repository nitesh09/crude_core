<aside class="main-sidebar" class="fixed-bottom">

  <section class="sidebar">

    <ul class="sidebar-menu" data-widget="tree">

      <li class=""><a href="form.php"><i class="fa fa-"></i> <span>Login Form</span></a></li>
      <li><a href="show_user.php"><i class="fa fa-"></i> <span>Show_User</span></a></li>
      <li class="treeview">
        <a href="#"><i class="fa fa-link"></i> <span>Multilevel</span>
          <span class="pull-right-container">
            <i class="fa fa-angle-left pull-right"></i>
          </span>
        </a>
        <ul class="treeview-menu">
          <li><a href="admin_add.php">Admin Add</a></li>
          <li><a href="#">Link in level 2</a></li>
        </ul>

      </li>
      <li class="header"  style="position: absolute;bottom: 0;width: 100%;">
        <p id="g-brandng" class="text-center">Design By <a class="gordinateur" href="http://gordinateur.com/" target="_blank">G-ordinateur</a></p>
      </li>

    </ul>

  </section>

</aside>