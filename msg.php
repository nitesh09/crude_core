<?php  
if(!empty($_SESSION['error'])) { ?>
<div class="row">
    <div class="col-md-12">
        <div class="alert alert-danger alert-dismissable">
            <button aria-hidden="true" data-dismiss="alert" class="close" type="button">x</button>
            <ul>
                <li>
                    <div><?php echo $_SESSION['error']; ?></div>
                </li>
            </ul>
        </div>
    </div>
</div>
<?php unset($_SESSION['error']); } ?>

<?php  if(!empty($_SESSION['success'])) { ?>
<div class="alert alert-success alert-dismissable">
    <button aria-hidden="true" data-dismiss="alert" class="close" type="button">x</button>
    <ul>
        <li>
            <div><?php echo $_SESSION['success']; ?></div>
        </li>
    </ul>
</div>
<?php unset($_SESSION['success']);  } ?>

<?php  
if(!empty($_SESSION['valid_error'])) { ?>
<div class="row">
    <div class="col-md-12">
        <div class="alert alert-danger alert-dismissable">
            <button aria-hidden="true" data-dismiss="alert" class="close" aria-label="close" type="button">&times;</button>
            <ul>
                <li>
                    <div><?php echo $_SESSION['valid_error']; ?></div>
                </li>
            </ul>
        </div>
    </div>
</div>
<?php unset($_SESSION['valid_error']); } ?>

<?php  
if(!empty($_SESSION['login_error'])) { ?>
<div class="row">
    <div class="col-md-12">
        <div class="alert alert-danger alert-dismissable">
            <button aria-hidden="true" data-dismiss="alert" class="close" aria-label="close" type="button">&times;</button>
            <ul>
                <li>
                    <div><?php echo $_SESSION['login_error']; ?></div>
                </li>
            </ul>
        </div>
    </div>
</div>
<?php unset($_SESSION['login_error']); } ?>