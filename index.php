<?php 
include "config.php";

if(!empty($_SESSION['email'])){
    header("location:show_user.php");
}

if(isset($_POST['login'])){
    $email = $_POST['email'];
    $password = $_POST['password'];
  //$password = md5 || base64_encode ($_POST['password']);

    //Form validation
    $valid_error='';
    if(!empty($_POST)){
        if(empty($email)){
            $valid_error.= "Email is required";
        }
        else{
            if (!filter_var($email, FILTER_VALIDATE_EMAIL)){
                $valid_error.="Email id should be in proper format.<br/>";
            }
        }
        if(empty($password)){
            $valid_error.= "Password is required.";
        }
        else{
            if (!preg_match("/^(?=.*\d)(?=.*[A-Za-z])[0-9A-Za-z!@#$%]{8,12}$/", $password)){
                $valid_error .= "Password should be in proper format.";
            }
        }
    }

    $login = mysqli_query($conn,"SELECT * FROM login WHERE email = '".$email."' and password = '".$password."' ");

    $no = mysqli_num_rows($login);
    // print_r($no);die();

    if($no == 1){
        $login_data = mysqli_fetch_assoc($login);

        $_SESSION['email'] = $login_data['email'];
        $_SESSION['password'] = $login_data['password'];

        header("location:form.php");
    }
    else {
        $_SESSION['error'] = 'Username or password is wrong.';
    }
}
?>

<html><head>
<meta charset="UTF-8">
<title>Form | Sign in</title>
<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
<!-- Bootstrap 3.3.5 -->
<link rel="stylesheet" href="../public/bootstrap/css/bootstrap.min.css">
<!-- Font Awesome -->
<link rel="stylesheet" href="../public/plugins/font-awesome-4.7.0/css/font-awesome.min.css">
<!-- Theme style -->
<link rel="stylesheet" href="../public/AdminLte/css/Admin.min.css">
<style type="text/css">
.gordinateur {
    color: #349ba8;
}

.logo {
    position: absolute;top: 30%;left: 20%;background: #fff;width: 250px;
}

.login-box {
    border:solid 1px #d5d3d4;
    background: #fff;
    margin: 12% 65%;
    box-shadow: 0px 0px 2px 1px #ccc;
}


@media(max-width: 1024px) {

    .login-box {
        margin: 15% 60%;
    }

    .logo {
        left: 10%;
    }

}

@media(min-width: 767px) and (max-width: 1023px) {

    .login-box {
        margin: 30% 46%;
        width: 50%;
    }

    .logo {
        left: 5%;
        width: 250px;
    }

}

@media(max-width: 767px) {
    .logo {
        left: 0;
        top: 16%;
        width: 150px;
        right: 0;
        margin: 0 auto;
    }   

    .login-box {
       margin: 80% 5%;
       width: 90%;
   }
}

</style>        
</head>

<body class="login-page" style="background: colour:blue;">
    <div class="login-box">
     <div class="login-logo">
        <a href="login.php"><b>Form</b> Login</a>
    </div><!-- /.login-logo -->
    <div class="login-box-body">
        <?php require_once('msg.php'); ?>

        <p class="login-box-msg">Sign in to start your session</p>
        <div class="row">
            <div id="error_div"></div>
        </div>
        <form name="form" action="" method="post">
            <div class="form-group has-feedback">
                <input type="hidden" name="backto" value="">

                <input type="text" name="email" class="form-control" data-validation="email" value="<?php //echo $_GET['email']; ?>" placeholder="Email">
                <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
            </div>
            <div class="form-group has-feedback">
                <input type="password" name="password" class="form-control" data-validation="strength" data-validation-strength="2" placeholder="Password">
                <span class="glyphicon glyphicon-lock form-control-feedback"></span>
            </div>
            <div class="row">
                <div class="col-xs-8"></div>
                <div class="col-xs-4">
                   <button type="submit" name="login" class="btn btn-primary btn-block btn-flat">Sign In</button>
               </div>
           </div>
       </form>

       <?php
       if(isset($_POST['submit'])){
        $email = $_POST['email'];
        $query = "SELECT * FROM login WHERE email = '$email' ";
        $run_query = mysqli_query($conn,$query);
        $row_query = mysqli_fetch_assoc($run_query);
        $_SESSION['user-email'] = $row_query['email'];
        $email_session = $_SESSION['user-email'];
        if(mysqli_num_rows($run_query)<1){

            $_SESSION['error'] = 'E-mail id is wrong.';
            header("location: index.php");
        }
        else
        {
           $_SESSION['success'] = 'E-mail id is correct.';
           header("location: cfm_otp.php");
       }
   }    
   ?>

   <br/><a href="javascript:void(0)" onclick="$(forgotpwd).modal('show');" class="pull-right">I forgot my password</a><br/>
</div>
<p id="g-branding" class="login-box-msg">Designed By <a class="gordinateur" href="#" target="_blank">Nitesh</a></p>
</div>
<!-- <img src="./dist/img/g-ordinateur.jpeg" height="130" width="150" class="logo"> -->

<!-- Modal -->
<div class="modal fade bs-example-modal-sm" id="forgotpwd" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title" id="myModalLabel">Forgot Password</h4>
            </div>
            <form name="fpass" novalidate="" action="" method="post">
                <div class="modal-body">
                    <div class="row">
                        <div id="show_error"></div>
                    </div>
                    <div class="form-group">
                        <label>Email <span class="text-danger">*</span></label>
                        <input type="email" class="form-control" id="email" data-validation="email" name="email" placeholder="Enter registered email" >
                    </div>
                </div>
                <div class="modal-footer">
                    <input type="submit" name="submit" class="btn btn-primary" value="Submit">
                </div>                      
            </form>
        </div>
    </div>
</div>

<script src="../public/plugins/jQuery/jQuery-2.1.4.min.js"></script>
<script src="../public/bootstrap/js/bootstrap.min.js"></script>
<script src="../public/js/validate.cus.js" type="text/javascript"></script>


<script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery-form-validator/2.3.26/jquery.form-validator.min.js"></script>
<script>
$.validate({
    modules : 'location, date, security, file',
    onModulesLoaded : function() {
      $('#country').suggestCountry();
  }
});

  // Restrict presentation length
  $('#presentation').restrictLength( $('#pres-max-length') );

  </script>

</body>
</html>