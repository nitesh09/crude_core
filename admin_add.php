<?php
include "config.php";
login();

if(isset($_POST['submit'])){

	$email = $_POST['email'];
	$password = $_POST['password'];

	$set = "INSERT INTO `login` (`email`,`password`) VALUES ('$email','$password') ";
	$set_query = mysqli_query($conn,$set);
	if ($set_query == TRUE){
		$_SESSION['success'] = "Admin Updated Successfully."; 
		header("location: admin_add.php");
	}
	else
	{
		echo "Unable To Insert Data";
	}
}
?>

<html><head>
<meta charset="UTF-8">
<title>Admin | Add</title>
<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
<!-- Bootstrap 3.3.5 -->
<link rel="stylesheet" href="http://livesilverprice.in/app/krsna_inv/public/bootstrap/css/bootstrap.min.css">
<!-- Font Awesome -->
<link rel="stylesheet" href="http://livesilverprice.in/app/krsna_inv/public/plugins/font-awesome-4.5.0/css/font-awesome.min.css">
<!-- Theme style -->
<link rel="stylesheet" href="http://livesilverprice.in/app/krsna_inv/public/dist/css/Admin.min.css">
<style type="text/css">
.gordinateur {
	color: #349ba8;
}

.logo {
	position: absolute;top: 30%;left: 20%;background: #fff;width: 250px;
}

.login-box {
	border:solid 1px #d5d3d4;
	background: #fff;
	margin: 8% 41%;
	box-shadow: 0px 0px 2px 1px #ccc;
}


@media(max-width: 1024px) {

	.login-box {
		margin: 15% 60%;
	}

	.logo {
		left: 10%;
	}

}

@media(min-width: 767px) and (max-width: 1023px) {

	.login-box {
		margin: 30% 46%;
		width: 50%;
	}

	.logo {
		left: 5%;
		width: 250px;
	}

}

@media(max-width: 767px) {
	.logo {
		left: 0;
		top: 16%;
		width: 150px;
		right: 0;
		margin: 0 auto;
	}   

	.login-box {
		margin: 80% 5%;
		width: 90%;
	}
}

</style>        
</head>
<body class="login-page" style="background: colour:blue;">

		<div class="login-box">
			<div class="login-logo">
				<a href="admin_add.php"><b>Admin</b> Update</a>
			</div><!-- /.login-logo -->
			<div class="login-box-body">
				<?php require_once('msg.php'); ?>
				<p class="login-box-msg">Sign in to start your session</p>
				<div class="row">
					<div id="error_div"></div>
				</div>
				<form name="loginform" action="" method="post">
					<div class="form-group has-feedback">
						<input type="hidden" name="backto" value="">
						<input type="text" name="email" class="form-control" data-validation="email" placeholder="Email">
						<span class="glyphicon glyphicon-envelope form-control-feedback"></span>
					</div>
					<div class="form-group has-feedback">
						<input type="password" name="password" class="form-control" data-validation="strength" 
						data-validation-strength="2" placeholder="Password">
						<span class="glyphicon glyphicon-lock form-control-feedback"></span>
					</div>
					<div class="row">
						<div class="col-xs-8"></div>
						<div class="col-xs-4">
							<button type="submit" name="submit" class="btn btn-primary btn-block btn-flat">Update</button>
						</div>
					</div>
				</form>
				
			</div>
			<p id="g-branding" class="login-box-msg">Designed By <a class="gordinateur" href="http://gordinateur.com/" target="_blank">G-Ordinateur Pvt. Ltd.</a></p>
		</div>
	<img src="" class="">

<script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery-form-validator/2.3.26/jquery.form-validator.min.js"></script>
<script>

  $.validate({
    modules : 'location, date, security, file',
    onModulesLoaded : function() {
      $('#country').suggestCountry();
    }
  });

  // Restrict presentation length
  $('#presentation').restrictLength( $('#pres-max-length') );
</script>

</script></body></html>