<?php
include "config.php";

// print_r($_POST);die();
if(isset($_POST['submit'])) {

	$session_email = $_SESSION['user-email'];
	$password = $_POST['password'];
	$cpassword = $_POST['cpassword'];


  $valid_error = '';
  if(empty($password)){
    $valid_error .= "Password is required <br/>";
  }
  else{
    if (!preg_match("/^(?=.*\d)(?=.*[A-Za-z])[0-9A-Za-z!@#$%]{8,12}$/", $password)){
     $valid_error .= "Password should be in proper format.<br/>";
   }
 }
 if(empty($cpassword)){
  $valid_error .= "Confirm Password is required";
}
else{
  if (!preg_match("/^(?=.*\d)(?=.*[A-Za-z])[0-9A-Za-z!@#$%]{8,12}$/", $cpassword)){
   $valid_error .= "Password should be in proper format.<br/>";
 }
}

if ($password != $cpassword){
  $valid_error = 'Password Not Match.<br/>';
}


    // If validation fail
if(!empty($valid_error)) {
  $_SESSION['error'] = $valid_error;
  header('location: fpass.php');die;
}


$query = "UPDATE `login` SET `password` = '$password',`cpassword` = '$cpassword'  WHERE `email` = '$session_email' ";	
$result = mysqli_query($conn,$query);

if($result){
  $_SESSION['success'] = 'Password Updated Successfully! Please Login Now.';
  header('location: index.php');die;
}
else{
  $_SESSION['error'] = 'Password not Updated.';
  header("location: fpass.php");die;
}
}
?>

<html><head>
<meta charset="UTF-8">
<title>Change | Password</title>
<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
<!-- Bootstrap 3.3.5 -->
<link rel="stylesheet" href="http://livesilverprice.in/app/krsna_inv/public/bootstrap/css/bootstrap.min.css">
<!-- Font Awesome -->
<link rel="stylesheet" href="http://livesilverprice.in/app/krsna_inv/public/plugins/font-awesome-4.5.0/css/font-awesome.min.css">
<!-- Theme style -->
<link rel="stylesheet" href="http://livesilverprice.in/app/krsna_inv/public/dist/css/Admin.min.css">
<style type="text/css">
.gordinateur {
  color: #349ba8;
}

.logo {
  position: absolute;top: 30%;left: 20%;background: #fff;width: 250px;
}

.login-box {
  border:solid 1px #d5d3d4;
  background: #fff;
  margin: 8% 41%;
  box-shadow: 0px 0px 2px 1px #ccc;
}


@media(max-width: 1024px) {

  .login-box {
    margin: 15% 60%;
  }

  .logo {
    left: 10%;
  }

}

@media(min-width: 767px) and (max-width: 1023px) {

  .login-box {
    margin: 30% 46%;
    width: 50%;
  }

  .logo {
    left: 5%;
    width: 250px;
  }

}

@media(max-width: 767px) {
  .logo {
    left: 0;
    top: 16%;
    width: 150px;
    right: 0;
    margin: 0 auto;
  }   

  .login-box {
   margin: 80% 5%;
   width: 90%;
 }
}


</style>        
</head>
<!-- <div class="container">
       <img src="dist\img\g-ordinateur.jpeg" style="margin-top: 15px" height="200px" width="400px"/> 
     </div> -->
     <body class="login-page" style="background: colour:blue;">
      <div class="login-box">
       <div class="login-logo">
        <a href="login.php"><b>Change</b> Password</a>
      </div><!-- /.login-logo -->
      <div class="login-box-body">

        <?php require_once('msg.php'); ?>
        <p class="login-box-msg">Change password to start your session</p>
        <div class="row">
          <div id="error_div"></div>
        </div>
        <form name="form" action="" method="post">
            <!-- <div class="form-group has-feedback">
                <input type="hidden" name="backto" value="">
                <input type="text" name="email" class="form-control" data-validation="email" value="<?php //echo $_GET['email']; ?>" placeholder="Email">
                <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
              </div> -->
              <div class="form-group has-feedback">
                <input type="password" name="password" class="form-control" data-validation="strength" data-validation-strength="2" placeholder="Password">
                <span class="glyphicon glyphicon-lock form-control-feedback"></span>
              </div>
              <div class="form-group has-feedback">
                <input type="password" name="cpassword" class="form-control" data-validation="strength" data-validation-strength="2" placeholder="Confirm Password">
                <span class="glyphicon glyphicon-lock form-control-feedback"></span>
              </div>
              <div class="row">
                <div class="col-xs-8"></div>
                <div class="col-xs-4">
                 <!--  <button class="btn btn-sm btn-basic"><a href="fpass.php">Forget Password?</a> -->
                 <button type="submit" name="submit" class="btn btn-primary btn-block btn-flat">Submit</button>
               </div><!-- /.col -->
             </div>
           </form>

           <script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
           <script src="//cdnjs.cloudflare.com/ajax/libs/jquery-form-validator/2.3.26/jquery.form-validator.min.js"></script>

           <!-- <br/><a href="javascript:void(0)" onclick="$(forgotpwd).modal('show');" class="pull-right">I forgot my password</a><br/> -->
         </div><!-- /.login-box-body -->
         <p id="g-branding" class="login-box-msg">Designed By <a class="gordinateur" href="http://gordinateur.com/" target="_blank">G-Ordinateur Pvt. Ltd.</a></p>
       </div><!-- /.login-box -->

       <script>

       $.validate({
        modules : 'location, date, security, file',
        onModulesLoaded : function() {
          $('#country').suggestCountry();
        }
      });

  // Restrict presentation length
  $('#presentation').restrictLength( $('#pres-max-length') );

  </script>

</body></html>