<?php require_once("config.php"); ?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <?php include("head.php"); ?>
        <title>Bootstrap Template</title>
        <meta content="" name="description">
        <meta content="" name="author">

        <style type="text/css">
            
        </style>
        
    </head>
    <body>
        <?php include("header.php"); ?>
        <section class="wrapper">
            <div class="container">
            <div class="main">
                <div class="logo-div">
                    <span class="icon-modal-logo"><span class="path1"></span><span class="path2"></span><span class="path3"></span><span class="path4"></span><span class="path5"></span></span>
                </div> 

                <div class="title-div">
                    <h1>Job Approval</h1>
                </div>
                <div class="clearfix"></div>

                <div class="detail-box">
                    <div class="customer-box">
                        <div class="title-box">
                            <div class="icon-box">
                                <span class="icon-effd"></span>
                            </div>
                            <h2>Customer Details</h2>
                        </div>

                        <ul class="list-unstyled">
                            <li><p>Name</p><span class="colon">:</span></li>
                            <li><small>Amol Khilari</small></li>
                            <li><p>Mobile Number</p><span class="colon">:</span></li>
                            <li><small>+91 1234567890</small></li>
                            <li><p>Address</p><span class="colon">:</span></li>
                            <li><small>C03, Kukreja Centre CBD Belapur</small></li>
                        </ul>
                    </div>
                    <div class="vehicle-box">
                         <div class="title-box">
                            <div class="icon-box">
                                <span class="icon-wqsd"></span>
                            </div>
                            <h2>Vehicle Details</h2>
                        </div>

                        <ul class="list-unstyled">
                            <li><p>Make</p><span class="colon">:</span></li>
                            <li><small>Honda</small></li>
                            <li><p>Model</p><span class="colon">:</span></li>
                            <li><small>CRV</small></li>
                            <li><p>Vehicle Number</p><span class="colon">:</span></li>
                            <li><small>1234569874</small></li>
                        </ul>


                    </div>
                    <div class="clearfix"></div>
                </div>

                <div class="job-box">
                    <div class="job-title">
                       <div class="icon-box">
                        <span class="icon-checklist"></span>
                    </div>
                    <h2>Job</h2>
                </div>
                </div>

                <div class="table-div">
                    <h4>Labour</h4><span class="yellow-sp"></span>

                    <div class="table-responsive">
                        <table class="table table-striped table-bordered">
                            <thead>
                                <tr>
                                    <th>Sr.no</th>
                                    <th>Iteam</th>
                                    <th>Rate</th>
                                    <th>Qty.</th>
                                    <th>Amount</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>John</td>
                                    <td>Doe</td>
                                    <td>john@example.com</td>
                                    <td>John</td>
                                    <td>Doe</td>
                                </tr>
                                 <tr>
                                    <td>John</td>
                                    <td>Doe</td>
                                    <td>john@example.com</td>
                                    <td>John</td>
                                    <td>Doe</td>
                                </tr>
                                 <tr>
                                    <td>John</td>
                                    <td>Doe</td>
                                    <td>john@example.com</td>
                                    <td>John</td>
                                    <td>Doe</td>
                                </tr>
                                <tr>
                                    <td>John</td>
                                    <td>Doe</td>
                                    <td>john@example.com</td>
                                    <td>John</td>
                                    <td>Doe</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>

                    <div class="total-box">
                        <div class="table-responsive">
                             <table class="table">
                            <tbody>
                                <tr>
                                    <td>Sub Total</td>
                                    <td>₹14,000</td>
                                </tr>
                                 <tr>
                                    <td>CGST</td>
                                    <td>6%</td>
                                    
                                </tr>
                                 <tr>
                                    <td>SGST</td>
                                    <td>6%</td>
                                </tr>
                                 <tr>
                                    <td>TOTAL</td>
                                    <td>₹30,000</td>
                                </tr>
                                
                            </tbody>
                        </table>
                        </div>
                    </div>

                    <div class="clearfix"></div>
                </div>
                <br>

                 <div class="table-div spare">
                    <h4>Spare Parts</h4><span class="yellow-sp"></span>

                    <div class="table-responsive">
                        <table class="table table-striped table-bordered">
                            <thead>
                                <tr>
                                    <th>Sr.no</th>
                                    <th>Iteam</th>
                                    <th>Rate</th>
                                    <th>Qty.</th>
                                    <th>Amount</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>John</td>
                                    <td>Doe</td>
                                    <td>john@example.com</td>
                                    <td>John</td>
                                    <td>Doe</td>
                                </tr>
                                 <tr>
                                    <td>John</td>
                                    <td>Doe</td>
                                    <td>john@example.com</td>
                                    <td>John</td>
                                    <td>Doe</td>
                                </tr>
                                 <tr>
                                    <td>John</td>
                                    <td>Doe</td>
                                    <td>john@example.com</td>
                                    <td>John</td>
                                    <td>Doe</td>
                                </tr>
                                <tr>
                                    <td>John</td>
                                    <td>Doe</td>
                                    <td>john@example.com</td>
                                    <td>John</td>
                                    <td>Doe</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>

                    <div class="total-box">
                        <div class="table-responsive">
                             <table class="table">
                            <tbody>
                                <tr>
                                    <td>Sub Total</td>
                                    <td>₹14,000</td>
                                </tr>
                                 <tr>
                                    <td>CGST</td>
                                    <td>6%</td>
                                    
                                </tr>
                                 <tr>
                                    <td>SGST</td>
                                    <td>6%</td>
                                </tr>
                                 <tr>
                                    <td>TOTAL</td>
                                    <td>₹30,000</td>
                                </tr>
                                
                            </tbody>
                        </table>
                        </div>
                    </div>

                    <div class="clearfix"></div>
                </div>

                <div class="total-amount">
                    <div class="block">
                      <p>Total Labour Amount</p> 
                      <h5>₹50,000</h5> 
                    </div>
                     <div class="block">
                      <p>Total Spares Amount</p> 
                      <h5>₹50,000</h5> 
                    </div>
                    <div class="block">
                      <p>Tax</p> 
                      <h5>12%</h5> 
                    </div>
                    <div class="block">
                      <p>Grand Total</p> 
                      <h5>₹60,000</h5> 
                    </div>
                
                </div>
                <div class="clearfix"></div>
                <br>
                <br>
                <div class="term-div">
                    <h4>Terms & Conditions</h4><span class="yellow-sp"></span> 
                    <p>Curabitur volutpat urna non purus hendrerit, scelerisque porta quam rhoncus.</p>
                    <p>Maecenas volutpat lacus sit amet tortor iaculis tincidunt.</p>
                    <p>Duis facilisis sem et ligula venenatis, vitae molestie turpis efficitur. Praesent consectetur nisi quis sapien mattis, sit amet scelerisque ipsum luctus.</p>
                    <p>Morbi et mi a velit feugiat aliquet aliquam varius ipsum.</p> 
                </div>
                <br>
                <br>

                <div class="button-div">
                    <button class="accept">
                        <span class="icon-success"></span>
                        <span class="text">Accept</span>
                    </button>
                    <button class="decline">
                        <span class="icon-error"></span>
                        <span class="text">Decline</span>
                    </button>
                </div>
            </div>
            </div>

        </section>
        <?php include("footer.php"); ?>
    </body>
</html>