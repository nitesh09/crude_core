<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">

<?php
	echo generate_style_link([
		'./css/bootstrap.min.css',
		'./css/font-awesome.min.css',
		'./css/bootstrap-custom.css',
		'./css/media.css',
		'./css/animate.min.css',
		'./css/aos.css',
		'./css/svg-icon.css',
	]);
?>
<!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
<![endif]-->

    