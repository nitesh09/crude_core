/* for active currant page */
$(function(){

	$("form").attr("autocomplete", "off");

	var url = window.location.href.split('?')[0];

	var str=location.href.toLowerCase();
  
	/* Remove query string */
	if(str.indexOf('?') > 0) {
	str = str.substring(0, str.indexOf('?'));
	}

	/* Remove # value */
	if(str.indexOf('#') > 0) {
	str = str.substring(0, str.indexOf('#'));
	}

	/* Remove trailing slases */
	str=str.replace(/\/$/, "");

	var element=$('.main-sidebar a[href$="'+str+'"]');
	if(element) {
		var parent=$(element).parent().parent();
		if($(parent).hasClass('sidebar-menu')) {
			$(element).parent().addClass('active'); 	
		}
		else {
			$(parent).parent().addClass('active');
			$(parent).css("display","block");
		}
		$(element).addClass('sub-active');
	}

	var width=$(window).width();
	if(width <= 767) {
		$('.date-mask').removeClass('datepicker');
		if($.fn.inputmask) {
	    	$('.date-mask').inputmask('remove');
	    }
	    $('.date-mask').attr('type','date');
	}

	/*var url = window.location.pathname;  
	var activePage = url.substring(url.lastIndexOf('/'));
	$('.main-sidebar a').each(function(){  
		var currentPage = this.href;
		currentPage =currentPage.substring(this.href.lastIndexOf('/'));
		if (activePage == currentPage) {
			var parent=$(this).parent().parent();
			if($(parent).hasClass('sidebar-menu')) {
				$(this).parent().addClass('active'); 	
			}
			else {
				$(parent).parent().addClass('active');
				$(parent).css("display","block");		
			}
		} 
	});*/

	$('#header-change-pass').click(function() {
		get_modaldata('Change password',base_url+"login/change_password");
	});

	/*$('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
		if($.fn.dataTable) {
			var table = $.fn.dataTable.fnTables(true);
			if( table.length > 0 ) {
				$(table).dataTable().fnAdjustColumnSizing();
			}
		}
	});*/

	/* Data table for listing pages */
	if($.fn.dataTable) {
		var height=parseInt($(window).height());
		var height=height-300;
		if(height < 500) {
			height=500;
		}

		var dataTable_opt={
			"bPaginate": false,
			"bsort": true,
			"scrollX": true,
			"scrollY": height+"px",
			"aaSorting": []
		}
		var oTable=$('#full-height-datatable').dataTable(dataTable_opt);
		/*$(document).on('keyup', '', function() {

		});*/
		$('#datatable-search').keyup(function(){
		    oTable.fnFilter(this.value);
		});

		var dataTable_obj=[];
		/* Multiple table within same page */
		dataTable_opt.scrollY=height-120+'px';
		$('.full-height-datatable').each(function(key, ele) {
			dataTable_obj[key]=$(ele).dataTable(dataTable_opt);
			/* ID of search box */
			console.log(dataTable_obj[key]);
			var textbox=$(dataTable_obj[key]).attr('data-searchbox');
			if(textbox) {
				$(textbox).keyup(function(){
				    dataTable_obj[key].fnFilter(this.value);
				});
			}
		});
	}

	/* Form validation library functions */
	if($.fn.validate) {


		// website url validation
		$.formUtils.addValidator({
			name : 'website',
			validatorFunction : function(value, $el, config, language, $form) {
				return value.match(/(^|\s)((https?:\/\/)?[\w-]+(\.[\w-]+)+\.?(:\d+)?(\/\S*)?)/gi);
			},
			errorMessage : 'Invalid website address.',
			errorMessageKey: 'website'
		});

		var form_element='.validate-form';
		jQuery.validate({
			
            modules : 'file, security',
			form:form_element,
			scrollToTopOnError:false,
			onError:function($form) {
				setTimeout(function() {
					$first_ele = $form.find('.has-error').first();
					var $ele=$first_ele.find('.form-control');
					$ele.focus();
				},100);
			}
        });

        /* Set custom message for select fields */
	    $('select',form_element).each(function() {
	    	this.oninvalid = function(e) {
	            e.target.setCustomValidity("");
	            if (!e.target.validity.valid) {
	            	var title = $(this).attr('title');
	            	var error="Please select valid from list";;	
	            	if(title) {
	            		error="Please select valid "+$(this).attr('data-title')+" from list";
	            	}
	                e.target.setCustomValidity(error);
	            }
	        };
	        this.oninput = function(e) {
	            e.target.setCustomValidity("");
	        };
	    });

	    /* ignore showing error on blur for first time  */
		var $file = $('input[type="file"]',form_element);
		$file.bind("blur", function(e) {
			var $ele=$(this)
		    setTimeout(function() {
		    	if($ele.val() == '') {
			    	$ele.parent().find('.form-error').remove();
			    	$ele.parent().removeClass('has-error');
			    	$ele.css({'border-color':''})
			    }
		    }, 100);
		});

		/* keyup catching will be changed back to body after selecting file */
		$file.bind("change", function(e) {
		    $(this).trigger('blur');
		});
	}

	/* Datepicker plugin */
	if($.fn.datepicker) {
		/*$('.datepicker').datepicker({
			format: 'dd-mm-yyyy',
			todayHighlight:true,
			autoclose:true,
		}).on('changeDate',function(e) {
			var ele=this;
			$(ele).trigger('blur');
		});*/

		$('.year-picker').datepicker({
			startView:'decade',
			minViewMode:'decade',
			format:'yyyy',
			autoclose:true,
		}).on('changeDate',function(e) {
			var ele=this;
			$(ele).trigger('blur');
		});

		$('.monthyear-picker').datepicker({
			startView:'month',
			minViewMode:'year',
			format:'mm-yyyy',
			autoclose:true,
		}).on('changeDate',function(e) {
			var ele=this;
			$(ele).trigger('blur');
		});
	}

	/* fix Datatable column issue in bootstrap tab */
	$('a[data-toggle="tab"]').on('shown.bs.tab', function(e){
		$($.fn.dataTable.tables(true)).DataTable()
		.columns.adjust();
		$('.select2-container').css('width','100%');
	}); 

	/* Select2 Class apply for .select */
	if($.fn.select2) {
		$('select.select2').select2();
	}

	/* Select2 issue fix in model */
	$('#search-model').on('show.bs.modal', function (e) {
	  $('#search-model').find('.select2-container').css('width','100%');
	  apply_datepicker();
	});

	/* Select2 issue fix in model */
	$('#commanModal').on('show.bs.modal', function (e) {
	  $('#commanModal').find('.select2-container').css('width','100%');
	  apply_datepicker();
	});

	/* For number filed that accept number with . */
	$(document).on('keypress','.number-field',function(evt) {
		evt = (evt) ? evt : window.event;
	    var charCode = (evt.which) ? evt.which : evt.keyCode;
	    if (charCode > 31 && (charCode < 48 || charCode > 57) && charCode !=46) {
	        return false;
	    }
	    return true;
	});

	$(document).on('keypress','.no-space',function(evt) {
		evt = (evt) ? evt : window.event;
		var charCode = (evt.which) ? evt.which : evt.keyCode;
	    if(charCode === 32) {
	        return false;
	    }
	    return true;
	});

	$(document).on('click','.table-list-edit',function() {
		var table=$(this).attr('data-tableid');
		var edit_url=$(this).attr('data-modelurl');
		var model_title=$(this).attr('data-modeltitle');
		var selected=get_all_selected(table);
		if(selected.length == 1) {
			var load_url=base_url+edit_url+'/'+selected[0];
			get_modaldata(model_title,load_url);
			
		}
		else if(selected.length > 1) {
			swal("Only one record allowed at a time");
		}
		else {
			swal("Please select at least one record");
		}
	});

	$(document).on('click','.table-list-editpage',function() {
		var table=$(this).attr('data-tableid');
		var edit_url=$(this).attr('data-modelurl');
		var model_title=$(this).attr('data-modeltitle');
		var selected=get_all_selected(table);
		if(selected.length == 1) {
			var load_url=base_url+edit_url+'/'+selected[0];
			window.location.href=load_url;
		}
		else if(selected.length > 1) {
			swal("Only one record allowed at a time");
		}
		else {
			swal("Please select at least one record");
		}
	});

	/* click on Delete button */
	$('.table-list-delete').click(function() {
		var table=$(this).attr('data-tableid');
	    var selected=get_all_selected(table);
	    var delete_url=$(this).attr('data-modelurl');
    
	    if(selected.length < 1) {
	      swal('Select at-least one record');
	    }
	    else {
	        var load_url=base_url+delete_url;
	        swal({
	            title: "Are you sure to delete?",
	            type: "warning",
	            showCancelButton: true,
	            confirmButtonColor: "#DD6B55",
	            confirmButtonText: "Yes, delete it!",
	            closeOnConfirm: true
	        }, function () {
	            loading();
	            $.ajax({
	                url: load_url,
	                type: 'POST',
	                data: {'ids':selected},
	                success : function(data){
	                    data=$.parseJSON(data);
	                    remove_loading();
	                    if(data.status == '1') {
	                        swal("Deleted!", data.message, "success");
	                        location.reload();
	                    }
	                    else {
	                        swal("Oops...", data.message, "error");
	                    }
	                }
	            });
	        });
	    }
	});

	/*    */
	$('input[name="item-search"]').keydown(function(e){
		if(e.which == 40) {
			var a=$(this).parent().find('.dropdown-menu a:first');
			$(a).focus()
		}
		return true;
	});

	$('.dropdown-menu a').keydown(function(e){
		switch(e.which){
			case 36: // home
	    	e.preventDefault();
	    	$(this).closest('.dropdown-menu').find('a:first').focus();
	      break;
			case 35: // end
	    	e.preventDefault();
	    	$(this).closest('.dropdown-menu').find('a:last').focus();
	      break;
	  	}
	});

	apply_datepicker();

	/* Open menu model in billing screen */
	$("#toggle-billing-menu").click(function() {
	  	$(this).toggleClass("on");
	  	$("#billing-menu").slideToggle();
	});

	// Item table carousel
	//$('.delivery-slider').carousel();

	// Billing table carousel 
	$(document).on('click','.carousel-control.handle-global', function() {
		$(this).parent().carousel($(this).attr('data-slide'));
	});

	/*if($.fn.autocomplete) {
		$("#global_search").autocomplete({
	        source: base_url+"customer/get_customer_global/",
	        focus: function(event, ui) {
	        	//console.log(ui);
	          // prevent value inserted on focus
	          $('input[name="global_search"]').val(ui.item.label);
	        },
	        select:function(event, ui) {
	        	//alert(ui.item.id);
	        	//if(ui.item.type=="customer") {
	        		window.location.href=base_url+"customer/customer_detail/"+ui.item.id; 
	        	//}
	        	
	        },
	    });
	}*/

	$( function() {
		$.widget("custom.catcomplete", $.ui.autocomplete, {
			_create: function() {
				this._super();
				this.widget().menu( "option", "items", "> :not(.ui-autocomplete-category)" );
			},
			_renderMenu: function( ul, items ) {
				var that = this,
				currentCategory = "";
				$.each( items, function( index, item ) {
					var li;
					if ( item.type != currentCategory ) {
						ul.append( "<li class='ui-autocomplete-category'>" + item.type + "</li>" );
						currentCategory = item.type;
					}
					li = that._renderItemData( ul, item );
				});
			}
		});

		$("#global_search").catcomplete({
			delay: 0,
			source: base_url+"customer/get_customer_global/",
			focus: function(event, ui) {
				$('input[name="global_search"]').val(ui.item.label);
			},
			select:function(event, ui) {
				//if(ui.item.type=="Customer") {
					window.location.href=base_url+"customer/customer_detail/"+ui.item.id; 
				//}
				
			},
		});
	});


});

function get_all_selected(table) {
    var checked_list = [];

	selector=$('#'+table).find('input:checkbox[name^=row_id]:checked');
    $(selector).each(function() {
        checked_list.push($(this).val());
    })
    return checked_list;
}

/* ajax waiting */
function loading() {
	over='<div id="overlay" style="position: fixed;left: 0;top: 0;bottom: 0;right: 0;background: #000;opacity: 0.8;filter: alpha(opacity=80);z-index: 9999;"><i style="position: absolute;top: 49%;left: 49%;font-size:37px;color:#fff;-webkit-animation: fa-spin 2s infinite linear;animation: fa-spin 2s infinite linear;" class="fa fa-refresh"></i></div>';
	$('body').append(over);
}

function remove_loading() {
	$('#overlay').remove();
}

function load_popup(title,data, modal_id) {
	if(!modal_id) {
		modal_id = '#commanModal';
	}
	$(modal_id+' .modal-title').html(title);
	$(modal_id+' .modal-body').html(data);
	$(modal_id).modal('show');
}

function get_modaldata(title, url, modal_id) {
	$.ajax({
		type:"POST",
		url:url,
		beforeSend:function() {
			loading();
		}
	})
	.done(function(data) {
		load_popup(title,data, modal_id);
		$('.chosen-container').css('width','240px');
	})
	.fail(function(jqXHR, textStatus) {
		alert( "Request failed: " + textStatus );
	})
	.always(function() {
		$('#overlay').remove();
	});
}

function round_decimle(num) {
 	return Math.round(num * 100) / 100
}


var date_format='dd-mm-yyyy';

function checkInput(type) {
    var input = document.createElement("input");
    input.setAttribute("type", type);
    return input.type == type;
}

/* Add date picker to .datepicker */
function apply_datepicker() {
	/* add datepicker to class .datepicker */
	if($.fn.datepicker) {
		$('.datepicker').each(function(argument) {

			if(checkInput('date')) {
				$(this).attr('type','date');
				return true;
			}
			
			var default_val=$(this).val();
			var ele=this;

			/* Change date format if value is not blank and in YYYY-MM-DD format */
			if(default_val!='' && default_val.match(/^(\d{4})-(\d{1,2})-(\d{1,2})$/) && default_val!='0000-00-00') {
				/* Convert date format */
				var d = new Date(default_val);
				var curr_date = d.getDate();
				if(curr_date < 10) {
					curr_date='0'+curr_date;
				}
				var curr_month = d.getMonth();
				curr_month++;

				if(curr_month < 10) {
					curr_month='0'+curr_month;
				}
				var curr_year = d.getFullYear();
				$(ele).val(curr_date+'-'+curr_month+'-'+curr_year);
			}

			if(default_val=='0000-00-00') {
				$(ele).val('');
			}

			$(this).datepicker({
			    "autoclose":true,
			    "format":date_format,
			    "autoUpdateInput":false,
			    "keepEmptyValues":true,
			    "todayHighlight":true,
			}).on('changeDate',function(e) {
				if($(ele).attr('data-minlimit')) {
					var limit_ele=$(ele).attr('data-minlimit');
					$(limit_ele).datepicker('setStartDate',$(ele).val());
				}
				else if($(ele).attr('data-maxlimit')) {
					var limit_ele=$(ele).attr('data-maxlimit');
					$(limit_ele).datepicker('setEndDate',$(ele).val());
				}
			});
			
		});

		$('.year-picker').datepicker({
			startView:'decade',
			minViewMode:'decade',
			format:'yyyy',
			autoclose:true,
		}).on('changeDate',function(e) {
			var ele=this;
			$(ele).trigger('blur');
		});

		$('.monthyear-picker').datepicker({
			startView:'month',
			minViewMode:'year',
			format:'MM-yyyy',
			autoclose:true,
		}).on('changeDate',function(e) {
			var ele=this;
			$(ele).trigger('blur');
		});
	}

	/* Read notification for current user */
	$('.notification_read').click(function() {
		$.ajax({
			url: window.base_url+'invoice/read_notification/',
			type: 'POST',
			success : function(data){
				data=$.parseJSON(data);
				if(data.status == '1') {
					$('.notifications-count').remove();
				}
			}
		});
	});
}


/* delete function */
function delete_record(id, url) {
	swal({
		title: "Are you sure to delete?",
		type: "warning",
		showCancelButton: true,
		confirmButtonColor: "#DD6B55",
		confirmButtonText: "Yes, delete it!",
		closeOnConfirm: true,
		allowOutsideClick:false
	}, function () {
		loading();
		$.ajax({
			url     : base_url+url,
			type    : 'POST',
			data    : {'id':id},
			success : function(data){
				data=$.parseJSON(data);
				remove_loading();
				if(data.status == '1') {
					swal({
						title: "Deleted!",
						html: data.message,
						type: "success",
						confirmButtonText: "OK",
						closeOnConfirm: true,
						closeOnCancel: false
					},
					function(isConfirm) {
						if(isConfirm) {
							window.location.reload();
						}
					});
				}
				else {
					swal("Oops...", data.message, "error");
				}
			}
		});
	}); 
}

$(document).ready(function() {	
	if($.fn.daterangepicker) {
		$('.date_range').daterangepicker({
			timePicker: true,
			autoUpdateInput: false,
			locale: {
				format: 'DD-MM-YYYY'
			},
			autoclose:true, 
			ranges: {
				'Today': [moment(), moment()],
				'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
				'This Week': [moment().startOf('week'), moment().endOf('week')],
				'Last Week': [moment().subtract(1, 'week').startOf('week'), moment().subtract(1, 'week').endOf('week')],
				'This Month': [moment().startOf('month'), moment().endOf('month')],
				'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')],
				'This Year': [moment().startOf('year'), moment().endOf('year')],
				'Last Year': [moment().subtract(1, 'year').startOf('year'), moment().subtract(1, 'year').endOf('year')],
			}
		});

		$('.date_range').on('apply.daterangepicker', function(ev, picker) {
			$(this).val(picker.startDate.format('DD-MM-YYYY') + ' - ' + picker.endDate.format('DD-MM-YYYY'));
		});

		$('.date_range').on('cancel.daterangepicker', function(ev, picker) {
			$(this).val('');
		});
	}
});