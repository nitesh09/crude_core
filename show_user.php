<?php
include 'config.php';
login();

include 'header.php';
include 'sidebar.php';

//delete Record
if(isset($_GET['del'])){
	$id = $_GET['del'];
	mysqli_query($conn, "DELETE FROM info WHERE id = $id ");
	$_SESSION['success'] = 'Data Deleted successfully.'; 
}

//Name Serach query
$query = 'SELECT * FROM info WHERE 1 = 1 ';
if(!empty($_GET['name'])){
	$query .=" and name like '%".$_GET['name']."%' ";
}

//Number Search query
if(!empty($_GET['number'])) {
	$query .=" and number =".$_GET['number']." ";
}

//State search query
if(!empty($_GET['state'])){
	$query .=" and state like '%".$_GET['state']."%' "; 
}

//Country search query
if(!empty($_GET['country'])){
	$query .=" and country like '%".$_GET['country']."%' ";
}

//Date search query
if(!empty($_GET['from_dob'])){
	$query .=" and dob >=   '".$_GET['from_dob']."'  ";
}

if(!empty($_GET['to_dob'])){
	$query .=" and dob  <=  '".$_GET['to_dob']."' ";
}

//Salary Search query
if(!empty($_GET['from_salary'])){
	$query .=" and salary >=   '".$_GET['from_salary']."'  ";
}

if(!empty($_GET['to_salary'])){
	$query .=" and salary  <=  '".$_GET['to_salary']."' ";
}

$rs = mysqli_query($conn, $query);
// print_r($query);die;

?>
<div class="content-wrapper" style="min-height: 800px;">
	<section class="content-header">
		<h1>
			Company Employee Details	 <small>
		</small>
	</h1>

</section>
<?php require_once('msg.php'); ?>

<div class="row">
	<div class="col-md-12">
		<section class="content">
			<!-- Your Page Content Here -->

			<!-- Button trigger modal -->

			<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static">
				<div class="modal-dialog">
					<div class="modal-content">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
							<h4 class="modal-title" id="myModalLabel">Search Company's Employee</h4>
						</div>
						<div class="modal-body">
							<form name="search_employee" method="get" action="show_user.php">
								<div class="row">
									<div class="col-md-12">
										<?php  ?>
										<div class="col-md-6">
											<label>Name </label>
											<input type="text" class="form-control" id="name" name="name"  placeholder="Enter Name" value="<?php if(!empty($_GET['name'])){echo $_GET['name'];} ?>" />						
										</div>
										<div class="col-md-6">
											<label>Mobile</label>
											<input type="text" class="form-control" id="number" name="number"  placeholder="Enter Mobile" value="<?php if(!empty($_GET['number'])){echo $_GET ['number'];} ?>" />
										</div>							
									</div>
									<div class="col-md-12" name="frmSearch">
										<div class="col-md-6">
											<label>From Date</label>
											<input type="date" class="form-control" id="dob" name="from_dob"  pattern="\d{4}-\d{1,2}-\d{1,2}" value="<?php if(!empty($_GET['from_dob'])){echo $_GET ['from_dob'];} ?>" />											
										</div>
										<div class="col-md-6">
											<label>To Date</label>
											<input type="date" class="form-control" id="dob" name="to_dob"  pattern="\d{4}-\d{1,2}-\d{1,2}"/ value="<?php if(!empty($_GET['to_dob'])){echo $_GET ['to_dob'];} ?>" >																				
										</div>			
									</div>
									<!-- <div class="col-md-12">
										<div class="col-md-6">
											<label for="state">State</label>
                                            <select name="state" class="form-control" id="state" value="">
                                                <option value="<?php echo $state; ?>"><?php if(!empty($_GET['state'])){echo $_GET ['state'];} ?></option>
                                                <option value="Andaman and Nicobar Islands">Andaman and Nicobar Islands</option>
                                                <option value="Andhra Pradesh">Andhra Pradesh</option>
                                                <option value="Andhra Pradesh (New)">Andhra Pradesh (New)</option>
                                                <option value="Arunachal Pradesh">Arunachal Pradesh</option>
                                                <option value="Assam">Assam</option>
                                                <option value="Bihar">Bihar</option>
                                                <option value="Chandigarh">Chandigarh</option>
                                                <option value="Chattisgarh">Chattisgarh</option>
                                                <option value="Dadra and Nagar Haveli">Dadra and Nagar Haveli</option>
                                                <option value="Daman and Diu">Daman and Diu</option>
                                                <option value="Delhi">Delhi</option>
                                                <option value="Goa">Goa</option>
                                                <option value="Gujarat">Gujarat</option>
                                                <option value="Haryana">Haryana</option>
                                                <option value="Himachal Pradesh">Himachal Pradesh</option>
                                                <option value="Jammu and Kashmir">Jammu and Kashmir</option>
                                                <option value="Jharkhand">Jharkhand</option>
                                                <option value="Karnataka">Karnataka</option>
                                                <option value="Kerala">Kerala</option>
                                                <option value="Lakshadweep Islands">Lakshadweep Islands</option>
                                                <option value="Madhya Pradesh">Madhya Pradesh</option>
                                                <option value="Maharashtra" >Maharashtra</option>
                                                <option value="Manipur">Manipur</option>
                                                <option value="Meghalaya">Meghalaya</option>
                                                <option value="Mizoram">Mizoram</option>
                                                <option value="Nagaland">Nagaland</option>
                                                <option value="Odisha">Odisha</option>
                                                <option value="Pondicherry">Pondicherry</option>
                                                <option value="Punjab">Punjab</option>
                                                <option value="Rajasthan">Rajasthan</option>
                                                <option value="Sikkim">Sikkim</option>
                                                <option value="Tamil Nadu">Tamil Nadu</option>
                                                <option value="Telangana">Telangana</option>
                                                <option value="Tripura">Tripura</option>
                                                <option value="Uttar Pradesh">Uttar Pradesh</option>
                                                <option value="Uttarakhand">Uttarakhand</option>
                                                <option value="West Bengal">West Bengal</option>
                                            </select>
										</div>			
										<div class="col-md-6">
											<label for="country">Country</label>
                                            <select name="country" class="form-control" id="country" style="text-transform: capitalize;" value="">
                                                <option value="<?php echo $country; ?>"><?php if(!empty($_GET['country'])){echo $_GET ['country'];} ?></option>
                                                <option value="united states">united states</option>
                                                <option value="canada">canada</option>
                                                <option value="afghanistan">afghanistan</option>
                                                <option value="albania">albania</option>
                                                <option value="algeria">algeria</option>
                                                <option value="american samoa">american samoa</option>
                                                <option value="andorra">andorra</option>
                                                <option value="angola">angola</option>
                                                <option value="anguilla">anguilla</option>
                                                <option value="antarctica">antarctica</option>
                                                <option value="antigua and/or barbuda">antigua and/or barbuda</option>
                                                <option value="argentina">argentina</option>
                                                <option value="armenia">armenia</option>
                                                <option value="aruba">aruba</option>
                                                <option value="australia">australia</option>
                                                <option value="austria">austria</option>
                                                <option value="azerbaijan">azerbaijan</option>
                                                <option value="bahamas">bahamas</option>
                                                <option value="bahrain">bahrain</option>
                                                <option value="bangladesh">bangladesh</option>
                                                <option value="barbados">barbados</option>
                                                <option value="belarus">belarus</option>
                                                <option value="belgium">belgium</option>
                                                <option value="belize">belize</option>
                                                <option value="benin">benin</option>
                                                <option value="bermuda">bermuda</option>
                                                <option value="bhutan">bhutan</option>
                                                <option value="bolivia">bolivia</option>
                                                <option value="bosnia and herzegovina">bosnia and herzegovina</option>
                                                <option value="botswana">botswana</option>
                                                <option value="bouvet island">bouvet island</option>
                                                <option value="brazil">brazil</option>
                                                <option value="british lndian ocean territory">british lndian ocean territory</option>
                                                <option value="brunei darussalam">brunei darussalam</option>
                                                <option value="bulgaria">bulgaria</option>
                                                <option value="burkina faso">burkina faso</option>
                                                <option value="burundi">burundi</option>
                                                <option value="cambodia">cambodia</option>
                                                <option value="cameroon">cameroon</option>
                                                <option value="cape verde">cape verde</option>
                                                <option value="cayman islands">cayman islands</option>
                                                <option value="central african republic">central african republic</option>
                                                <option value="chad">chad</option>
                                                <option value="chile">chile</option>
                                                <option value="china">china</option>
                                                <option value="christmas island">christmas island</option>
                                                <option value="cocos (keeling) islands">cocos (keeling) islands</option>
                                                <option value="colombia">colombia</option>
                                                <option value="comoros">comoros</option>
                                                <option value="congo">congo</option>
                                                <option value="cook islands">cook islands</option>
                                                <option value="costa rica">costa rica</option>
                                                <option value="croatia (hrvatska)">croatia (hrvatska)</option>
                                                <option value="cuba">cuba</option>
                                                <option value="cyprus">cyprus</option>
                                                <option value="czech republic">czech republic</option>
                                                <option value="denmark">denmark</option>
                                                <option value="djibouti">djibouti</option>
                                                <option value="dominica">dominica</option>
                                                <option value="dominican republic">dominican republic</option>
                                                <option value="east timor">east timor</option>
                                                <option value="ecuador">ecuador</option>
                                                <option value="egypt">egypt</option>
                                                <option value="el salvador">el salvador</option>
                                                <option value="equatorial guinea">equatorial guinea</option>
                                                <option value="eritrea">eritrea</option>
                                                <option value="estonia">estonia</option>
                                                <option value="ethiopia">ethiopia</option>
                                                <option value="falkland islands (malvinas)">falkland islands (malvinas)</option>
                                                <option value="faroe islands">faroe islands</option>
                                                <option value="fiji">fiji</option>
                                                <option value="finland">finland</option>
                                                <option value="france">france</option>
                                                <option value="france, metropolitan">france, metropolitan</option>
                                                <option value="french guiana">french guiana</option>
                                                <option value="french polynesia">french polynesia</option>
                                                <option value="french southern territories">french southern territories</option>
                                                <option value="gabon">gabon</option>
                                                <option value="gambia">gambia</option>
                                                <option value="georgia">georgia</option>
                                                <option value="germany">germany</option>
                                                <option value="ghana">ghana</option>
                                                <option value="gibraltar">gibraltar</option>
                                                <option value="greece">greece</option>
                                                <option value="greenland">greenland</option>
                                                <option value="grenada">grenada</option>
                                                <option value="guadeloupe">guadeloupe</option>
                                                <option value="guam">guam</option>
                                                <option value="guatemala">guatemala</option>
                                                <option value="guinea">guinea</option>
                                                <option value="guinea-bissau">guinea-bissau</option>
                                                <option value="guyana">guyana</option>
                                                <option value="haiti">haiti</option>
                                                <option value="heard and mc donald islands">heard and mc donald islands</option>
                                                <option value="honduras">honduras</option>
                                                <option value="hong kong">hong kong</option>
                                                <option value="hungary">hungary</option>
                                                <option value="iceland">iceland</option>
                                                <option value="india" >india</option>
                                                <option value="indonesia">indonesia</option>
                                                <option value="iran (islamic republic of)">iran (islamic republic of)</option>
                                                <option value="iraq">iraq</option>
                                                <option value="ireland">ireland</option>
                                                <option value="israel">israel</option>
                                                <option value="italy">italy</option>
                                                <option value="ivory coast">ivory coast</option>
                                                <option value="jamaica">jamaica</option>
                                                <option value="japan">japan</option>
                                                <option value="jordan">jordan</option>
                                                <option value="kazakhstan">kazakhstan</option>
                                                <option value="kenya">kenya</option>
                                                <option value="kiribati">kiribati</option>
                                                <option value="korea, democratic people's republic of">korea, democratic people's republic of</option>
                                                <option value="korea, republic of">korea, republic of</option>
                                                <option value="kosovo">kosovo</option>
                                                <option value="kuwait">kuwait</option>
                                                <option value="kyrgyzstan">kyrgyzstan</option>
                                                <option value="lao people's democratic republic">lao people's democratic republic</option>
                                                <option value="latvia">latvia</option>
                                                <option value="lebanon">lebanon</option>
                                                <option value="lesotho">lesotho</option>
                                                <option value="liberia">liberia</option>
                                                <option value="libyan arab jamahiriya">libyan arab jamahiriya</option>
                                                <option value="liechtenstein">liechtenstein</option>
                                                <option value="lithuania">lithuania</option>
                                                <option value="luxembourg">luxembourg</option>
                                                <option value="macau">macau</option>
                                                <option value="macedonia">macedonia</option>
                                                <option value="madagascar">madagascar</option>
                                                <option value="malawi">malawi</option>
                                                <option value="malaysia">malaysia</option>
                                                <option value="maldives">maldives</option>
                                                <option value="mali">mali</option>
                                                <option value="malta">malta</option>
                                                <option value="marshall islands">marshall islands</option>
                                                <option value="martinique">martinique</option>
                                                <option value="mauritania">mauritania</option>
                                                <option value="mauritius">mauritius</option>
                                                <option value="mayotte">mayotte</option>
                                                <option value="mexico">mexico</option>
                                                <option value="micronesia, federated states of">micronesia, federated states of</option>
                                                <option value="moldova, republic of">moldova, republic of</option>
                                                <option value="monaco">monaco</option>
                                                <option value="mongolia">mongolia</option>
                                                <option value="montenegro">montenegro</option>
                                                <option value="montserrat">montserrat</option>
                                                <option value="morocco">morocco</option>
                                                <option value="mozambique">mozambique</option>
                                                <option value="myanmar">myanmar</option>
                                                <option value="namibia">namibia</option>
                                                <option value="nauru">nauru</option>
                                                <option value="nepal">nepal</option>
                                                <option value="netherlands">netherlands</option>
                                                <option value="netherlands antilles">netherlands antilles</option>
                                                <option value="new caledonia">new caledonia</option>
                                                <option value="new zealand">new zealand</option>
                                                <option value="nicaragua">nicaragua</option>
                                                <option value="niger">niger</option>
                                                <option value="nigeria">nigeria</option>
                                                <option value="niue">niue</option>
                                                <option value="norfork island">norfork island</option>
                                                <option value="northern mariana islands">northern mariana islands</option>
                                                <option value="norway">norway</option>
                                                <option value="oman">oman</option>
                                                <option value="pakistan">pakistan</option>
                                                <option value="palau">palau</option>
                                                <option value="panama">panama</option>
                                                <option value="papua new guinea">papua new guinea</option>
                                                <option value="paraguay">paraguay</option>
                                                <option value="peru">peru</option>
                                                <option value="philippines">philippines</option>
                                                <option value="pitcairn">pitcairn</option>
                                                <option value="poland">poland</option>
                                                <option value="portugal">portugal</option>
                                                <option value="puerto rico">puerto rico</option>
                                                <option value="qatar">qatar</option>
                                                <option value="reunion">reunion</option>
                                                <option value="romania">romania</option>
                                                <option value="russian federation">russian federation</option>
                                                <option value="rwanda">rwanda</option>
                                                <option value="saint kitts and nevis">saint kitts and nevis</option>
                                                <option value="saint lucia">saint lucia</option>
                                                <option value="saint vincent and the grenadines">saint vincent and the grenadines</option>
                                                <option value="samoa">samoa</option>
                                                <option value="san marino">san marino</option>
                                                <option value="sao tome and principe">sao tome and principe</option>
                                                <option value="saudi arabia">saudi arabia</option>
                                                <option value="senegal">senegal</option>
                                                <option value="serbia">serbia</option>
                                                <option value="seychelles">seychelles</option>
                                                <option value="sierra leone">sierra leone</option>
                                                <option value="singapore">singapore</option>
                                                <option value="slovakia">slovakia</option>
                                                <option value="slovenia">slovenia</option>
                                                <option value="solomon islands">solomon islands</option>
                                                <option value="somalia">somalia</option>
                                                <option value="south africa">south africa</option>
                                                <option value="south georgia south sandwich islands">south georgia south sandwich islands</option>
                                                <option value="spain">spain</option>
                                                <option value="sri lanka">sri lanka</option>
                                                <option value="st. helena">st. helena</option>
                                                <option value="st. pierre and miquelon">st. pierre and miquelon</option>
                                                <option value="sudan">sudan</option>
                                                <option value="suriname">suriname</option>
                                                <option value="svalbarn and jan mayen islands">svalbarn and jan mayen islands</option>
                                                <option value="swaziland">swaziland</option>
                                                <option value="sweden">sweden</option>
                                                <option value="switzerland">switzerland</option>
                                                <option value="syrian arab republic">syrian arab republic</option>
                                                <option value="taiwan">taiwan</option>
                                                <option value="tajikistan">tajikistan</option>
                                                <option value="tanzania, united republic of">tanzania, united republic of</option>
                                                <option value="thailand">thailand</option>
                                                <option value="togo">togo</option>
                                                <option value="tokelau">tokelau</option>
                                                <option value="tonga">tonga</option>
                                                <option value="trinidad and tobago">trinidad and tobago</option>
                                                <option value="tunisia">tunisia</option>
                                                <option value="turkey">turkey</option>
                                                <option value="turkmenistan">turkmenistan</option>
                                                <option value="turks and caicos islands">turks and caicos islands</option>
                                                <option value="tuvalu">tuvalu</option>
                                                <option value="uganda">uganda</option>
                                                <option value="ukraine">ukraine</option>
                                                <option value="united arab emirates">united arab emirates</option>
                                                <option value="united kingdom">united kingdom</option>
                                                <option value="united states minor outlying islands">united states minor outlying islands</option>
                                                <option value="uruguay">uruguay</option>
                                                <option value="uzbekistan">uzbekistan</option>
                                                <option value="vanuatu">vanuatu</option>
                                                <option value="vatican city state">vatican city state</option>
                                                <option value="venezuela">venezuela</option>
                                                <option value="vietnam">vietnam</option>
                                                <option value="virgin islands (british)">virgin islands (british)</option>
                                                <option value="virgin islands (u.s.)">virgin islands (u.s.)</option>
                                                <option value="wallis and futuna islands">wallis and futuna islands</option>
                                                <option value="western sahara">western sahara</option>
                                                <option value="yemen">yemen</option>
                                                <option value="yugoslavia">yugoslavia</option>
                                                <option value="zaire">zaire</option>
                                                <option value="zambia">zambia</option>
                                                <option value="zimbabwe">zimbabwe</option>
                                            </select>
										</div>
									</div> -->
									<div class="col-md-12">
										<div class="col-md-6">
											<label>From Salary</label>
											<input type="text" class="form-control" id="from_salary" name="from_salary" placeholder="Salary"/ value="<?php if(!empty($_GET['from_salary'])){echo $_GET ['from_salary'];} ?>" >											
										</div>
										<div class="col-md-6">
											<label>To Salary</label>
											<input type="text" class="form-control" id="to_salary" name="to_salary" placeholder="Salary"/ value="<?php if(!empty($_GET['to_salary'])){echo $_GET ['to_salary'];} ?>" >																				
										</div>			
									</div>
								</div>

								<div class="row"><br/>
									<div class="modal-footer">
										<input type="submit" name="search"  class="btn btn-primary" value="Search">
										<input type="reset" name="reset"  class="btn btn-default" value="Reset">
										<button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
									</div>							      			
								</div>							      			
							</form>
						</div>
					</div>
				</div>
			</div>	

			<div class="panel panel-default">
				<div class="panel-heading">
					<a href="show_user.php" class="fa fa-fw fa-refresh" onclick="window.location.reload( true );" data-toggle="tooltip" data-placement="top" title="Refresh"></a>
					<a href="#" data-target="#myModal" class="fa fa-fw fa-filter" onclick="$(searchemployee).modal('show');"  data-placement="top" data-toggle="modal" title="" data-original-title="Filter Data"></a>
					<a href="form.php" class="fa fa-fw fa-plus" data-toggle="tooltip" data-placement="top" title="ADD Employee"></a>
					<!-- <a href="form.php?edit=<?php echo $row['id']; ?>" class="fa fa-fw fa-edit" id="editemployee" data-toggle="tooltip" data-placement="top" title="Edit Employee"></a>
					<a href="#" class="fa fa-fw fa-trash-o" id="deleteall" class="fa fa-fw fa-trash-o" data-toggle="tooltip" data-placement="top" title="Delete Employee"></a> -->
					<!-- <a href="#" class="fa fa-fw fa-search-minus" data-toggle="tooltip" data-placement="top" title="" data-original-title="Clear Filter"></a> -->
					<span class="paginations"></span>
				</div>	
				<div class="panel-body">
					<div class="box-body table-responsive">
						<div id="DataTables_Table_0_wrapper" class="dataTables_wrapper no-footer">
						  <!-- <div id="DataTables_Table_0_filter" class="dataTables_filter">
                                      <label>Search:<input type="text" id="myInput" onkeyup="myFunction()" class="" placeholder="" ></label></div> -->
                                      <div class="dataTables_scroll"><div class="dataTables_scrollHead" style="overflow: hidden; position: relative; border: 0px; width: 100%;">
                                       <div class="dataTables_scrollHeadInner" style="margin-top: 2px;" "box-sizing: content-box; width: 1591px; padding-right: 0px;"></div>
                                       <table class="table table-bordered table-striped dataTable no-footer" role="grid" style="margin-left: 0px; width: 1591px;">
                                        <thead>

                                         <div class="container-fluid">
                                          <table class="table table-hover" id="order_data">
                                           <tr role="row">
                                            <th width="5%" class="sorting" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" style="width: 74px;" aria-label="#: activate to sort column ascending">#</th>
                                            <th width="20%" class="sorting" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" style="width: 220px;" aria-label="Name: activate to sort column ascending">Name</th>
                                            <th width="20%" class="sorting" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" style="width: 238px;" aria-label="Company: activate to sort column ascending">Date-Of-Birth</th>
                                            <th width="10%" class="sorting" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" style="width: 189px;" aria-label="Mobile: activate to sort column ascending">Mobile</th>
                                            <th width="15%" class="sorting" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" style="width: 243px;" aria-label="Email: activate to sort column ascending">Email</th>
                                            <th width="5%" class="sorting" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" style="width: 63px;" aria-label="Balance: activate to sort column ascending">Balance</th>
                                      </tr>
                                      <?php while ($row = mysqli_fetch_assoc($rs)){ ?>  
                                      <tr>
                                            <td>
                                             <label>
                                              <input type="checkbox" id="checkID[]" name="checkID[]" href="form.php?edit=<?php echo $row['id']; ?>" class="selectAll">
                                        </label>				
                                  </td>
                                  <td><?php echo $row['name']; ?></td> 
                                  <td><?php echo $row['dob']; ?></td>
                                  <td><?php echo $row['number']; ?></td>
                                  <td><?php echo $row['email']; ?></td>
                                  <td><?php echo $row['salary']; ?></td>
                                  <td>
                                       <button><a href="form.php?id=<?php echo $row['id'];?>">Update</a></button>
                                 </td>
                                 <td><button onclick="cfm(#ITEMID,#ITEMNAME)"><a href="show_user.php?del=<?php echo $row['id'];?>">Delete</a></button>
                                 </td>
                           </tr>
                           <?php } ?>
                     </thead>
               </table>
         </div>
   </div>
</div>			
</div>


<link href="http://livesilverprice.in/app/krsna_inv/public/plugins/datatables/dataTables.bootstrap.css" rel="stylesheet" type="text/css">
<link href="http://livesilverprice.in/app/krsna_inv/public/plugins/datepicker/datepicker3.css" rel="stylesheet" type="text/css">

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery-form-validator/2.3.26/jquery.form-validator.min.js"></script>
</div>
</div>

<?php
include 'footer.php';
unset($_SESSION['old_data']);
?>