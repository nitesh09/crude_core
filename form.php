<?php
include 'config.php';
login();

include 'header.php';
include 'sidebar.php';


if(isset($_GET['id']) || !empty($_SESSION['old_data'])){
    if(isset($_GET['id'])){
        $id = $_GET['id'];
    }
    $rec = mysqli_query($conn, "SELECT * FROM info WHERE `id`='$id' ");
    $record = mysqli_fetch_assoc($rec);

    if(!empty($_SESSION['old_data'])){
        $record = $_SESSION['old_data'];
    }
    
    $name = $record['name'];
    $number = $record['number'];
    $email = $record['email'];
    $alt_email = $record['alt_email'];
    $dob = $record['dob'];
    $gst = $record['gst'];
    $address1 = $record['address1'];
    $address2 = $record['address2'];
    $city = $record['city'];
    $zipc = $record['zipc'];
    $state = $record['state'];
    $country = $record['country'];
    $salary = $record['salary'];
    $note = $record['note'];
    if(!empty($id)){
        $id = $record['id'];
// var_dump($record);die;
    }
}

function get_all_data($conn){
    $query = "SELECT * FROM info";
    $result = mysqli_query($conn, $query);
    return mysqli_num_rows($result);
}
?>

<div class="content-wrapper" style="min-height: 800px;">
    <section class="content-header">
        <h1>
            Add Employee
        </h1>
        <?php require_once('msg.php'); ?>

        <div class="box box-primary">
            <div class="box-body">
                <div class="row">
                    <form method="POST" enctype="multipart/form-data" action="method.php">
                        <div class="col-md-12" id="moduleerror" style="display:none;">
                            <div class="alert alert-danger alert-dismissable">
                                <button aria-hidden="true" data-dismiss="alert" class="close" type="button">x</button>
                                At least One Module Access must be give!
                            </div>
                        </div>

                        <div class="col-md-12">
                            <div class="col-md-12">
                                <div class="col-md-12">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="clearfix"></div>

                                            <?php 
                                            if(!empty($record['id'])) {  
                                                echo '<input type="hidden" name="id" value="'.$id.'">';    
                                            } 
                                            ?>

                                            <div class="col-md-6">
                                              <label for="name">Person Name <span class="text-danger">*</span> </label>
                                              <input type="text" class="form-control" name="name" placeholder="Enter Name" data-validation="length alphanumeric" data-validation-length="3-12" data-validation-error-msg="User name has to be an alphanumeric value (3-12 chars)" value="<?php if(!empty($name)){echo $name;}?>">
                                          </div>
                                          <div class="col-md-6">
                                            <label for="number">Contact Number</label>
                                            <input type="number" class="form-control number-field" id="mobile" name="number" data-validation="number" maxlength="12" placeholder="Enter Mobile" value="<?php if(!empty($number)){echo $number;}?>"> 
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="box-header">
                                        <h3 class="box-title">Contact Details</h3>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="col-md-6">
                                            <label for="email">Email <span class="text-danger">*</span> </label>
                                            <input type="text" class="form-control" id="email" name="email" data-validation="email" placeholder="Enter Email" value="<?php if(!empty($email)){echo $email;}?>">
                                        </div>
                                        <div class="col-md-6">
                                            <label for="alt_email">Alternate Email</label>
                                            <input type="text" class="form-control" id="sec_email" name="alt_email" data-validation="email" placeholder="Enter Alternate Email" value="<?php if(!empty($alt_email)){echo $alt_email;}?>">
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="col-md-6">
                                            <label for="dob">Date of Birth</label>
                                            <input type="text" class="form-control" id="dob" name="dob" placeholder="yyyy-mm-dd" data-validation="birthdate" data-validation-help="yyyy-mm-dd"  value="<?php if(!empty($dob)){echo $dob;}?>">
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="col-md-6">
                                            <label for="gst">GST No.</label>
                                            <input type="text" class="form-control" id="" name="gst" maxlength="15" placeholder="" value="<?php if(!empty($gst)){echo $gst;}?>"> 
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="box-header">
                                        <h3 class="box-title">Address Details</h3>
                                    </div>
                                    
                                    <div class="col-md-12">
                                        <div class="col-md-6">
                                            <label for="address1">Address Line 1</label>
                                            <input type="text" class="form-control" id="address1" name="address1" placeholder="Enter Address Line 1" value="<?php if(!empty($address1)){echo $address1;}?>"> 
                                        </div>
                                        <div class="col-md-6">
                                            <label for="address2">Address Line 2</label>
                                            <input type="text" class="form-control" id="address2" name="address2" placeholder="Enter Address Line 2" value="<?php if(!empty($address2)){echo $address2;}?>"> 
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="col-md-3">
                                            <label for="city">City</label>
                                            <input type="text" class="form-control" id="city" name="city" placeholder="Enter City" value="<?php if(!empty($city)){echo $city;}?>"> 
                                        </div>
                                        <div class="col-md-3">
                                            <label for="zipc">Zip-code</label>
                                            <input type="number" class="form-control number-field" id="zipcode" maxlength="7" name="zipc"   title="Should be in Number format" maxlength="7" placeholder="Enter Zipcode" value="<?php if(!empty($zipc)){echo $zipc;}?>"> 
                                        </div>
                                        <div class="col-md-3">
                                            <label for="state">State</label>
                                            <select name="state" class="form-control" id="state">

                                                <option value="<?php echo $state; ?>"><?php if(!empty($state)){echo $state;}?></option>
                                                <option value="Andaman and Nicobar Islands">Andaman and Nicobar Islands</option>
                                                <option value="Andhra Pradesh">Andhra Pradesh</option>
                                                <option value="Andhra Pradesh (New)">Andhra Pradesh (New)</option>
                                                <option value="Arunachal Pradesh">Arunachal Pradesh</option>
                                                <option value="Assam">Assam</option>
                                                <option value="Bihar">Bihar</option>
                                                <option value="Chandigarh">Chandigarh</option>
                                                <option value="Chattisgarh">Chattisgarh</option>
                                                <option value="Dadra and Nagar Haveli">Dadra and Nagar Haveli</option>
                                                <option value="Daman and Diu">Daman and Diu</option>
                                                <option value="Delhi">Delhi</option>
                                                <option value="Goa">Goa</option>
                                                <option value="Gujarat">Gujarat</option>
                                                <option value="Haryana">Haryana</option>
                                                <option value="Himachal Pradesh">Himachal Pradesh</option>
                                                <option value="Jammu and Kashmir">Jammu and Kashmir</option>
                                                <option value="Jharkhand">Jharkhand</option>
                                                <option value="Karnataka">Karnataka</option>
                                                <option value="Kerala">Kerala</option>
                                                <option value="Lakshadweep Islands">Lakshadweep Islands</option>
                                                <option value="Madhya Pradesh">Madhya Pradesh</option>
                                                <option value="Maharashtra" >Maharashtra</option>
                                                <option value="Manipur">Manipur</option>
                                                <option value="Meghalaya">Meghalaya</option>
                                                <option value="Mizoram">Mizoram</option>
                                                <option value="Nagaland">Nagaland</option>
                                                <option value="Odisha">Odisha</option>
                                                <option value="Pondicherry">Pondicherry</option>
                                                <option value="Punjab">Punjab</option>
                                                <option value="Rajasthan">Rajasthan</option>
                                                <option value="Sikkim">Sikkim</option>
                                                <option value="Tamil Nadu">Tamil Nadu</option>
                                                <option value="Telangana">Telangana</option>
                                                <option value="Tripura">Tripura</option>
                                                <option value="Uttar Pradesh">Uttar Pradesh</option>
                                                <option value="Uttarakhand">Uttarakhand</option>
                                                <option value="West Bengal">West Bengal</option>
                                            </select>
                                        </div>
                                        <div class="col-md-3">
                                            <label for="country">Country</label>
                                            <select name="country" class="form-control" id="country" style="text-transform: capitalize;">
                                                <option value="<?php echo $country; ?>"><?php if(!empty($country)){echo $country;}?></option>
                                                <option value="united states">united states</option>
                                                <option value="canada">canada</option>
                                                <option value="afghanistan">afghanistan</option>
                                                <option value="albania">albania</option>
                                                <option value="algeria">algeria</option>
                                                <option value="american samoa">american samoa</option>
                                                <option value="andorra">andorra</option>
                                                <option value="angola">angola</option>
                                                <option value="anguilla">anguilla</option>
                                                <option value="antarctica">antarctica</option>
                                                <option value="antigua and/or barbuda">antigua and/or barbuda</option>
                                                <option value="argentina">argentina</option>
                                                <option value="armenia">armenia</option>
                                                <option value="aruba">aruba</option>
                                                <option value="australia">australia</option>
                                                <option value="austria">austria</option>
                                                <option value="azerbaijan">azerbaijan</option>
                                                <option value="bahamas">bahamas</option>
                                                <option value="bahrain">bahrain</option>
                                                <option value="bangladesh">bangladesh</option>
                                                <option value="barbados">barbados</option>
                                                <option value="belarus">belarus</option>
                                                <option value="belgium">belgium</option>
                                                <option value="belize">belize</option>
                                                <option value="benin">benin</option>
                                                <option value="bermuda">bermuda</option>
                                                <option value="bhutan">bhutan</option>
                                                <option value="bolivia">bolivia</option>
                                                <option value="bosnia and herzegovina">bosnia and herzegovina</option>
                                                <option value="botswana">botswana</option>
                                                <option value="bouvet island">bouvet island</option>
                                                <option value="brazil">brazil</option>
                                                <option value="british lndian ocean territory">british lndian ocean territory</option>
                                                <option value="brunei darussalam">brunei darussalam</option>
                                                <option value="bulgaria">bulgaria</option>
                                                <option value="burkina faso">burkina faso</option>
                                                <option value="burundi">burundi</option>
                                                <option value="cambodia">cambodia</option>
                                                <option value="cameroon">cameroon</option>
                                                <option value="cape verde">cape verde</option>
                                                <option value="cayman islands">cayman islands</option>
                                                <option value="central african republic">central african republic</option>
                                                <option value="chad">chad</option>
                                                <option value="chile">chile</option>
                                                <option value="china">china</option>
                                                <option value="christmas island">christmas island</option>
                                                <option value="cocos (keeling) islands">cocos (keeling) islands</option>
                                                <option value="colombia">colombia</option>
                                                <option value="comoros">comoros</option>
                                                <option value="congo">congo</option>
                                                <option value="cook islands">cook islands</option>
                                                <option value="costa rica">costa rica</option>
                                                <option value="croatia (hrvatska)">croatia (hrvatska)</option>
                                                <option value="cuba">cuba</option>
                                                <option value="cyprus">cyprus</option>
                                                <option value="czech republic">czech republic</option>
                                                <option value="denmark">denmark</option>
                                                <option value="djibouti">djibouti</option>
                                                <option value="dominica">dominica</option>
                                                <option value="dominican republic">dominican republic</option>
                                                <option value="east timor">east timor</option>
                                                <option value="ecuador">ecuador</option>
                                                <option value="egypt">egypt</option>
                                                <option value="el salvador">el salvador</option>
                                                <option value="equatorial guinea">equatorial guinea</option>
                                                <option value="eritrea">eritrea</option>
                                                <option value="estonia">estonia</option>
                                                <option value="ethiopia">ethiopia</option>
                                                <option value="falkland islands (malvinas)">falkland islands (malvinas)</option>
                                                <option value="faroe islands">faroe islands</option>
                                                <option value="fiji">fiji</option>
                                                <option value="finland">finland</option>
                                                <option value="france">france</option>
                                                <option value="france, metropolitan">france, metropolitan</option>
                                                <option value="french guiana">french guiana</option>
                                                <option value="french polynesia">french polynesia</option>
                                                <option value="french southern territories">french southern territories</option>
                                                <option value="gabon">gabon</option>
                                                <option value="gambia">gambia</option>
                                                <option value="georgia">georgia</option>
                                                <option value="germany">germany</option>
                                                <option value="ghana">ghana</option>
                                                <option value="gibraltar">gibraltar</option>
                                                <option value="greece">greece</option>
                                                <option value="greenland">greenland</option>
                                                <option value="grenada">grenada</option>
                                                <option value="guadeloupe">guadeloupe</option>
                                                <option value="guam">guam</option>
                                                <option value="guatemala">guatemala</option>
                                                <option value="guinea">guinea</option>
                                                <option value="guinea-bissau">guinea-bissau</option>
                                                <option value="guyana">guyana</option>
                                                <option value="haiti">haiti</option>
                                                <option value="heard and mc donald islands">heard and mc donald islands</option>
                                                <option value="honduras">honduras</option>
                                                <option value="hong kong">hong kong</option>
                                                <option value="hungary">hungary</option>
                                                <option value="iceland">iceland</option>
                                                <option value="india" >india</option>
                                                <option value="indonesia">indonesia</option>
                                                <option value="iran (islamic republic of)">iran (islamic republic of)</option>
                                                <option value="iraq">iraq</option>
                                                <option value="ireland">ireland</option>
                                                <option value="israel">israel</option>
                                                <option value="italy">italy</option>
                                                <option value="ivory coast">ivory coast</option>
                                                <option value="jamaica">jamaica</option>
                                                <option value="japan">japan</option>
                                                <option value="jordan">jordan</option>
                                                <option value="kazakhstan">kazakhstan</option>
                                                <option value="kenya">kenya</option>
                                                <option value="kiribati">kiribati</option>
                                                <option value="korea, democratic people's republic of">korea, democratic people's republic of</option>
                                                <option value="korea, republic of">korea, republic of</option>
                                                <option value="kosovo">kosovo</option>
                                                <option value="kuwait">kuwait</option>
                                                <option value="kyrgyzstan">kyrgyzstan</option>
                                                <option value="lao people's democratic republic">lao people's democratic republic</option>
                                                <option value="latvia">latvia</option>
                                                <option value="lebanon">lebanon</option>
                                                <option value="lesotho">lesotho</option>
                                                <option value="liberia">liberia</option>
                                                <option value="libyan arab jamahiriya">libyan arab jamahiriya</option>
                                                <option value="liechtenstein">liechtenstein</option>
                                                <option value="lithuania">lithuania</option>
                                                <option value="luxembourg">luxembourg</option>
                                                <option value="macau">macau</option>
                                                <option value="macedonia">macedonia</option>
                                                <option value="madagascar">madagascar</option>
                                                <option value="malawi">malawi</option>
                                                <option value="malaysia">malaysia</option>
                                                <option value="maldives">maldives</option>
                                                <option value="mali">mali</option>
                                                <option value="malta">malta</option>
                                                <option value="marshall islands">marshall islands</option>
                                                <option value="martinique">martinique</option>
                                                <option value="mauritania">mauritania</option>
                                                <option value="mauritius">mauritius</option>
                                                <option value="mayotte">mayotte</option>
                                                <option value="mexico">mexico</option>
                                                <option value="micronesia, federated states of">micronesia, federated states of</option>
                                                <option value="moldova, republic of">moldova, republic of</option>
                                                <option value="monaco">monaco</option>
                                                <option value="mongolia">mongolia</option>
                                                <option value="montenegro">montenegro</option>
                                                <option value="montserrat">montserrat</option>
                                                <option value="morocco">morocco</option>
                                                <option value="mozambique">mozambique</option>
                                                <option value="myanmar">myanmar</option>
                                                <option value="namibia">namibia</option>
                                                <option value="nauru">nauru</option>
                                                <option value="nepal">nepal</option>
                                                <option value="netherlands">netherlands</option>
                                                <option value="netherlands antilles">netherlands antilles</option>
                                                <option value="new caledonia">new caledonia</option>
                                                <option value="new zealand">new zealand</option>
                                                <option value="nicaragua">nicaragua</option>
                                                <option value="niger">niger</option>
                                                <option value="nigeria">nigeria</option>
                                                <option value="niue">niue</option>
                                                <option value="norfork island">norfork island</option>
                                                <option value="northern mariana islands">northern mariana islands</option>
                                                <option value="norway">norway</option>
                                                <option value="oman">oman</option>
                                                <option value="pakistan">pakistan</option>
                                                <option value="palau">palau</option>
                                                <option value="panama">panama</option>
                                                <option value="papua new guinea">papua new guinea</option>
                                                <option value="paraguay">paraguay</option>
                                                <option value="peru">peru</option>
                                                <option value="philippines">philippines</option>
                                                <option value="pitcairn">pitcairn</option>
                                                <option value="poland">poland</option>
                                                <option value="portugal">portugal</option>
                                                <option value="puerto rico">puerto rico</option>
                                                <option value="qatar">qatar</option>
                                                <option value="reunion">reunion</option>
                                                <option value="romania">romania</option>
                                                <option value="russian federation">russian federation</option>
                                                <option value="rwanda">rwanda</option>
                                                <option value="saint kitts and nevis">saint kitts and nevis</option>
                                                <option value="saint lucia">saint lucia</option>
                                                <option value="saint vincent and the grenadines">saint vincent and the grenadines</option>
                                                <option value="samoa">samoa</option>
                                                <option value="san marino">san marino</option>
                                                <option value="sao tome and principe">sao tome and principe</option>
                                                <option value="saudi arabia">saudi arabia</option>
                                                <option value="senegal">senegal</option>
                                                <option value="serbia">serbia</option>
                                                <option value="seychelles">seychelles</option>
                                                <option value="sierra leone">sierra leone</option>
                                                <option value="singapore">singapore</option>
                                                <option value="slovakia">slovakia</option>
                                                <option value="slovenia">slovenia</option>
                                                <option value="solomon islands">solomon islands</option>
                                                <option value="somalia">somalia</option>
                                                <option value="south africa">south africa</option>
                                                <option value="south georgia south sandwich islands">south georgia south sandwich islands</option>
                                                <option value="spain">spain</option>
                                                <option value="sri lanka">sri lanka</option>
                                                <option value="st. helena">st. helena</option>
                                                <option value="st. pierre and miquelon">st. pierre and miquelon</option>
                                                <option value="sudan">sudan</option>
                                                <option value="suriname">suriname</option>
                                                <option value="svalbarn and jan mayen islands">svalbarn and jan mayen islands</option>
                                                <option value="swaziland">swaziland</option>
                                                <option value="sweden">sweden</option>
                                                <option value="switzerland">switzerland</option>
                                                <option value="syrian arab republic">syrian arab republic</option>
                                                <option value="taiwan">taiwan</option>
                                                <option value="tajikistan">tajikistan</option>
                                                <option value="tanzania, united republic of">tanzania, united republic of</option>
                                                <option value="thailand">thailand</option>
                                                <option value="togo">togo</option>
                                                <option value="tokelau">tokelau</option>
                                                <option value="tonga">tonga</option>
                                                <option value="trinidad and tobago">trinidad and tobago</option>
                                                <option value="tunisia">tunisia</option>
                                                <option value="turkey">turkey</option>
                                                <option value="turkmenistan">turkmenistan</option>
                                                <option value="turks and caicos islands">turks and caicos islands</option>
                                                <option value="tuvalu">tuvalu</option>
                                                <option value="uganda">uganda</option>
                                                <option value="ukraine">ukraine</option>
                                                <option value="united arab emirates">united arab emirates</option>
                                                <option value="united kingdom">united kingdom</option>
                                                <option value="united states minor outlying islands">united states minor outlying islands</option>
                                                <option value="uruguay">uruguay</option>
                                                <option value="uzbekistan">uzbekistan</option>
                                                <option value="vanuatu">vanuatu</option>
                                                <option value="vatican city state">vatican city state</option>
                                                <option value="venezuela">venezuela</option>
                                                <option value="vietnam">vietnam</option>
                                                <option value="virgin islands (british)">virgin islands (british)</option>
                                                <option value="virgin islands (u.s.)">virgin islands (u.s.)</option>
                                                <option value="wallis and futuna islands">wallis and futuna islands</option>
                                                <option value="western sahara">western sahara</option>
                                                <option value="yemen">yemen</option>
                                                <option value="yugoslavia">yugoslavia</option>
                                                <option value="zaire">zaire</option>
                                                <option value="zambia">zambia</option>
                                                <option value="zimbabwe">zimbabwe</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        
                        <div class="col-md-12">
                            <div class="col-md-12">

                                <div class="col-md-3">
                                    <label for="salary">Monthly Salary<span class="text-danger">*</span></label>
                                    <input type="text" class="form-control number-field" id="id_salary_span" name="salary" data-validation="number" placeholder="Enter Monthly Salary" value="<?php if(!empty($salary)){echo $salary;}?>">         
                                </div>
                                <div class="col-md-6">
                                    <label for="note">Note</label>
                                    <textarea type="text"  class="form-control" id="note" name="note" placeholder="Enter Note" value=""><?php if(!empty($note)){echo $note;}?></textarea>
                                    <br>
                                    <input type="hidden" name="companyid" id="companyid" value="0">           
                                </div>
                            </div>
                        </div>
                        <div id="info" style="display: none;">
                            <div class="col-md-12">
                                <div class="box-header">
                                    <h3 class="box-title">Login's Details</h3>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="col-md-12">
                                    <div class="col-md-4">
                                        <label>Role<span class="text-danger">*</span></label>
                                        <select name="role" class="form-control">
                                            <option value="" selected="">SELECT</option>
                                            <option value="admin">Admin</option>
                                            <option value="user">user</option>
                                        </select>
                                    </div>
                                    <div class="col-md-4">
                                        <label>Password <span class="text-danger">*</span></label>
                                        <input type="password" class="form-control" id="password" name="password" placeholder="Enter Password"> 
                                    </div>
                                    <div class="col-md-4">
                                        <label>Confirm Password <span class="text-danger">*</span></label>
                                        <input type="password" class="form-control" id="cfpassword" name="cfpassword" placeholder="Enter confirm Password"> 
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <br>     
                            <div class="modal-footer">
                                <button type="submit" name="submit" value="submit" class="btn btn-primary" style="margin-top:10px;">Submit</button>
                                <input type="reset" name="reset" class="btn btn-default" value="Reset" style="margin-top:10px;">
                                <input href="show_user.php" type="button" name="close" id="close" class="btn btn-danger" value="Close" style="margin-top:10px;">
                            </div>
                        </div>
                    </div>
                </form>
                <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
                <link rel="stylesheet" href="/resources/demos/style.css">

                <script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
                <script src="//cdnjs.cloudflare.com/ajax/libs/jquery-form-validator/2.3.26/jquery.form-validator.min.js"></script>
                <script>

        $.validate({
            modules : 'location, date, security, file',
            onModulesLoaded : function() {
              $('#country').suggestCountry();
          }
      });

  // Restrict presentation length
  $('#presentation').restrictLength( $('#pres-max-length') );

  $(document).ready(function(){
      $("#myInput").on("keyup", function() {
        var value = $(this).val().toLowerCase();
        $("#myTable tr").filter(function() {
          $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
      });
    });
  });

  $(document).ready(function(){

    $('.input-daterange').datepicker({
        todayBtn:'linked',
        format: "yyyy-mm-dd",
        autoclose: true
    });
    fetch_data('no');
    function fetch_data(is_date_search, start_date='', end_date='')
    {
        var dataTable = $('#order_data').DataTable({
            "processing" : true,
            "serverSide" : true,
            "order" : [],
            "ajax" : {
                url:"fetch.php",
                type:"POST",
                data:{
                    is_date_search:is_date_search, start_date:start_date, end_date:end_date
                }
            }
        });
    }

    </script>
</div>
</div>
</section>
</div>
</div>
<?php include 'footer.php'; ?>

<?php 
unset($_SESSION['old_data']);
?>